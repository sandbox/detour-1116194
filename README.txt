nodejs_tic
By Joe Turgeon [http://arithmetric.com]


This is an example module that uses the Node.js integration module (nodejs) to
implement a multi-player Tic Tac Toe game in a Drupal site.


Requirements:

The Node.js integration module must be installed and configured.


Installation:

1. Patch the Node.js integration module with the patch for server extensions at:
http://drupal.org/node/1116186

2. Copy this module into your sites/all/modules directory (or equivalent).

3. Edit your nodejs.config.js configuration file for the node server to specify
the nodejs_tic.server.js as a server extension. For example:

'extensions': ['../nodejs_tic/nodejs_tic.logic.js', '../nodejs_tic/nodejs_tic.server.js']

4. Restart your node server.
