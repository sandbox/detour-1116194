/* Server-side JavaScript for nodejs_tic */

var ticGame = require(__dirname + "/nodejs_tic.logic.js"),
  // game data
  players = {},
  games = [],
  playerQueue = [],
  // functions from server.js
  publishMessageToChannel,
  publishMessageToClient,
  addClientToChannel;

exports.setup = function (config) {
  publishMessageToChannel = config.publishMessageToChannel;
  publishMessageToClient = config.publishMessageToClient;
  addClientToChannel = config.addClientToChannel;
  process.on('client-message', handleMessage).on('client-disconnect', cleanupPlayer);
};

function cleanupGame(gid) {
  console.log('cleanupGame ' + gid);

  // Check that the game exists
  if (!games[gid]) {
    return;
  }

  // Remove game from players
  var num = games[gid].players.length;
  for (var i = 0; i < num; i++) {
    var sessionId = games[gid].players[i];
    if (players.hasOwnProperty(sessionId) && players[sessionId].games) {
      var idx = players[sessionId].games.indexOf(gid);
      players[sessionId].games.splice(idx, 1);
    }
  }

  // Delete game data
  delete(games[gid]);
}

function cleanupPlayer(sessionId) {
  console.log('nodejs_tic: cleanupPlayer ' + sessionId);

  // Check that player is registered
  if (!players.hasOwnProperty(sessionId)) {
    return;
  }

  // Ensure player is not in queue
  var idx = playerQueue.indexOf(sessionId);
  if (idx >= 0) {
    playerQueue.splice(idx, 1);
  }

  // Handle player's active games
  var num = players[sessionId].games.length;
  for (var i = 0; i < num; i++) {
    var gid = players[sessionId].games[i];
    if (games[gid] && games[gid].players.length) {
      // Notify other player
      var p1Id = games[gid].players[0] == sessionId ? games[gid].players[1] : games[gid].players[0];
      if (players.hasOwnProperty(p1Id)) {
        publishMessageToClient(p1Id, {type: 'disconnect'});
      }

      // Clean up game
      cleanupGame(gid);
    }
  }

  // Delete player data
  delete(players[sessionId]);
}

function handleMessage(sessionId, message) {
  var type = message.type,
    jsobj,
    jsmsg,
    jsstr,
    p1Id,
    game,
    gameOver = false;

  if (type == 'join') {
    players[sessionId] = {games: []};
    if (!playerQueue.length) {
      playerQueue.push(sessionId);
      jsmsg = {type: 'wait'};
      publishMessageToClient(sessionId, jsmsg);
    }
    else {
      p1Id = playerQueue.shift();
      ggame = new ticGame.game();
      gplayers = [p1Id, sessionId];
      if (Math.round(Math.random())) {
        gplayers.reverse();
      }
      gid = games.length;
      gchannel = 'nodejs_tic_game_' + gid;
      games[gid] = {channel: gchannel, players: gplayers, game: ggame};
      players[sessionId].games.push(gid);
      addClientToChannel(sessionId, gchannel);
      players[p1Id].games.push(gid);
      addClientToChannel(p1Id, gchannel);

      jsmsg = {type: 'game', state: 'start', localPlayer: (gplayers.indexOf(sessionId) + 1), nextPlayer: ggame.nextPlayer()};
      publishMessageToClient(sessionId, jsmsg);

      jsmsg = {type: 'game', state: 'start', localPlayer: (gplayers.indexOf(p1Id) + 1), nextPlayer: ggame.nextPlayer()};
      publishMessageToClient(p1Id, jsmsg);
    }
  }
  else if (type == 'move' && message.hasOwnProperty('x') && message.hasOwnProperty('y')) {
    if (players.hasOwnProperty(sessionId) && players[sessionId].games.length) {
      gid = players[sessionId].games[0];
      if (games[gid]) {
        game = games[gid].game;
        console.log('move: game spaces are [' + game.getSpaces().join() + ']');
        if (game.moveValid(message.x, message.y) && game.nextPlayer() == (games[gid].players.indexOf(sessionId) + 1)) {
          game.move(message.x, message.y);
          if (game.getState() != game.STATE_PLAY) {
            jsmsg = {type: 'game', state: 'end', moves: game.getMoves(), spaces: game.getSpaces(), gameState: game.getState(), playerWin: game.getPlayerWin()};
            gameOver = true;
          }
          else {
            jsmsg = {type: 'game', state: 'move', nextPlayer: game.nextPlayer(), moves: game.getMoves(), spaces: game.getSpaces(), gameState: game.getState()};
          }
        }
        else {
          jsmsg = {type: 'game', state: 'illegal', nextPlayer: game.nextPlayer(), moves: game.getMoves(), spaces: game.getSpaces(), gameState: game.getState()};
        }
        jsmsg.channel = games[gid].channel;
        publishMessageToChannel(jsmsg);
        if (gameOver) {
          cleanupGame(gid);
        }
      }
    }
  }
}
