/* Client-side JavaScript for nodejs_tic */

(function ($) {

window.nodejsTic = window.nodejsTic || (function () {
  var STATE_PRE_GAME = 0,
    STATE_WAIT = 1,
    STATE_GAME = 2,
    STATE_DONE = 3,
    state = STATE_PRE_GAME,
    $wrapper,
    $info,
    $canvas,
    ctx,
    cwidth = 320,
    cheight = 320,
    lwidth = 10,
    lcolor = '#888',
    xcolor = '#e66',
    ocolor = '#6e6',
    scolor = '#333',
    hoverCoord = false,
    game,
    localPlayer;

  /* client UI code */

  function drawMark(x, y, type, hover) {
    var bwidth = cwidth / 3,
      mwidth = Math.ceil(bwidth * 2 / 3),
      mborder = Math.ceil(bwidth / 6),
      mshadowX = Math.ceil(bwidth / 32),
      mshadowY = Math.ceil(bwidth / 16);
    ctx.strokeStyle = (type == 2) ? ocolor : xcolor;
    ctx.shadowOffsetX = hover ? mshadowX : 0;
    ctx.shadowOffsetY = hover ? mshadowY : 0;
    ctx.shadowBlur = hover ? 10 : 0;
    ctx.shadowColor = scolor;
    if (type == 1) { /* X mark */
      ctx.beginPath();
      ctx.moveTo(bwidth * x + mborder, bwidth * y + mborder);
      ctx.lineTo(bwidth * (x + 1) - mborder, bwidth * (y + 1) - mborder);
      ctx.moveTo(bwidth * (x + 1) - mborder, bwidth * y + mborder);
      ctx.lineTo(bwidth * x + mborder, bwidth * (y + 1) - mborder);
      ctx.stroke();
    }
    else if (type == 2) { /* O mark */
      ctx.beginPath();
      ctx.arc(bwidth * (x + 0.5), bwidth * (y + 0.5), mwidth / 2, 0, Math.PI * 2, false);
      ctx.stroke();
    }
  }

  function drawBoard() {
    var bwidth = Math.floor(cwidth / 3),
      moves,
      num,
      i;

    ctx.clearRect(0, 0, cwidth, cheight);
    ctx.lineWidth = lwidth;
    ctx.strokeStyle = lcolor;
    ctx.shadowColor = ctx.shadowBlur = ctx.shadowOffsetX = ctx.shadowOffsetY = 0;
    ctx.beginPath();
    ctx.moveTo(bwidth, 0);
    ctx.lineTo(bwidth, cheight);
    ctx.moveTo(bwidth * 2, 0);
    ctx.lineTo(bwidth * 2, cheight);
    ctx.moveTo(0, bwidth);
    ctx.lineTo(cwidth, bwidth);
    ctx.moveTo(0, bwidth * 2);
    ctx.lineTo(cwidth, bwidth * 2);
    ctx.stroke();

    moves = game.getMoves();
    num = moves.length;
    for (i = 0; i < num; i++) {
      if (moves[i].hasOwnProperty('x') && moves[i].hasOwnProperty('y')) {
        drawMark(moves[i].x, moves[i].y, (i % 2) ? 2 : 1, false);
      }
    }
  }

  function getBoardCoord(x, y) {
    return {x: Math.floor(x / cwidth * 3), y: Math.floor(y / cheight * 3)};
  }

  function boardMouse(e) {
    var coffset = $canvas.offset(),
      ex = e.pageX - coffset.left,
      ey = e.pageY - coffset.top,
      coord = getBoardCoord(ex, ey);
    if (state != STATE_GAME) {
      return;
    }
    if (e.type === 'click' && game.nextPlayer() == localPlayer && game.moveValid(coord.x, coord.y)) {
      game.move(coord.x, coord.y);
      nodeSend({
        type: 'move',
        x: coord.x,
        y: coord.y
      });
      hoverCoord = false;
      drawBoard();
    }
    else if (e.type === 'mouseleave') {
      drawBoard();
      hoverCoord = false;
    }
    else if (e.type === 'mousemove' && (!hoverCoord || !(coord.x == hoverCoord.x && coord.y == hoverCoord.y))) {
      drawBoard();
      if (game.nextPlayer() == localPlayer && game.moveValid(coord.x, coord.y)) {
        drawMark(coord.x, coord.y, game.nextPlayer(), true);
      }
      hoverCoord = coord;
    }
  }

  function infoMouse(e) {
    if (state == STATE_PRE_GAME || state == STATE_DONE) {
      nodeSend({type: 'join'});
      $info.unbind('click');
    }
  }

  function uiSetup() {
    if (!$wrapper || !$wrapper.length) {
      $wrapper = $('#nodejs-tic-wrapper');
      $canvas = $('<canvas id="nodejs-tic-canvas" width="' + cwidth + '" height="' + cheight + '">' + Drupal.t('Browser support for the canvas element is required.') + '</canvas>');
      ctx = $canvas.get(0).getContext('2d');
      $info = $('<div id="nodejs-tic-info"></div>');
      $wrapper.empty().append($canvas).append($info);
    }

    drawBoard();
    $canvas.mousemove(boardMouse).mouseleave(boardMouse).click(boardMouse);
    showInfo('Waiting for the server');
  }

  function showInfo(txt, clickable) {
    $info.html(txt).css({
      left: (cwidth - $info.outerWidth()) / 2 + 'px',
      top: (cheight - $info.outerHeight()) / 2 + 'px',
      'max-width': Math.floor(cwidth * 3/4) + 'px',
      opacity: 1,
      cursor: clickable ? 'pointer' : ''
    }).show();
  }

  /* node.js code */

  function nodeSend(message) {
    Drupal.Nodejs.socket.send(message);
  }

  function nodeCallback(message) {
/*    var str,
      key;
    str = message.hasOwnProperty('broadcast') ? 'broadcast ' : '';
    str += 'message received';
    str += message.hasOwnProperty('channel') ? ' on channel ' + message.channel : '';
    str += ': ';
    for (key in message) {
      if (key != 'broadcast' && key != 'channel' && message.hasOwnProperty(key)) {
        str += key + ': ' + message[key] + ' ';
      }
    }
    alert(str);
*/

    if (message.hasOwnProperty('status') && message.status == 'authenticated') {
      showInfo('Click Here to Play!', true);
      $info.click(infoMouse);
    }
    else if (message.hasOwnProperty('type')) {
      var type = message.type;
      if (type == 'wait') {
        state = STATE_WAIT;
        showInfo('Waiting for another player ...');
      }
      else if (type == 'game') {
        var mstate = message.state;
        if (mstate == 'start') {
          localPlayer = message.localPlayer;
          state = STATE_GAME;
          showInfo('Starting Game: ' + (localPlayer == game.nextPlayer() ? 'Your Move' : 'Their Move'));
          if (localPlayer == game.nextPlayer()) {
            $info.animate({'opacity': 0}, 500, 'linear', function () { $info.hide(); });
          }
        }
        else if (mstate == 'move' || mstate == 'invalid') {
          game.setMoves(message.moves);
          game.setSpaces(message.spaces);
          game.nextPlayer(message.nextPlayer);
          drawBoard();
          var str = (localPlayer == game.nextPlayer() ? 'Your Move' : 'Their Move');
          showInfo(str);
          if (localPlayer == game.nextPlayer()) {
            $info.animate({'opacity': 0}, 500, 'linear', function () { $info.hide(); });
          }
        }
        else if (mstate == 'end') {
          var gstate = message.gameState;
          game.setMoves(message.moves);
          game.setSpaces(message.spaces);
          drawBoard();
          state = STATE_DONE;
          if (gstate == game.STATE_WIN) {
            if (message.playerWin == localPlayer) {
              showInfo('<strong>You won!</strong><br/>Great job!');
            }
            else {
              showInfo('<strong>You lost.</strong><br/>Try again.');
            }
          }
          else if (gstate == game.STATE_NO_WIN) {
            showInfo("<strong>Cat's game!</strong><br/>It's a tie.");
          }
        }
      }
      else if (type == 'disconnect') {
        showInfo('The other player left the game.');
        state = STATE_DONE;
      }
    }
  }

  /* initialization */

  function setup() {
    if (!window.nodejsTicGame) {
      return;
    }
    game = new nodejsTicGame();
    uiSetup();
    Drupal.Nodejs.callbacks.nodejsTic = {callback: nodeCallback};
  }

  return {
    'setup': setup
  };

}());

Drupal.behaviors.nodejsTic = Drupal.behaviors.nodejsTic || {attach: window.nodejsTic.setup};

}(jQuery));
