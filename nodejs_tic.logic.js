/* Game logic JavaScript for nodejs_tic */

var nodejsTicGame = nodejsTicGame || function () {
  var STATE_PLAY = 0,
    STATE_WIN = 1,
    STATE_NO_WIN = 2,
    player = 1,
    playerWin = false,
    moves = [],
    spaces = [],
    state = STATE_PLAY,
    callbacks = [];

  /* game ops */

  function move(x, y) {
    var pos = (y * 3) + x;
    moves.push({x: x, y: y, player: player});
    spaces[pos] = player;
    player = (player == 2) ? 1 : 2;
    updateState();
  }

  /* game states */

  function updateState() {
    var i, j;

    if (state != STATE_PLAY) {
      return;
    }

    // Check if there is a win scenario
    for (i = 0; i < 3; i++) {
      // Check for win by column
      if (spaces[i] && spaces[i] == spaces[i + 3] && spaces[i] == spaces[i + 6]) {
        playerWin = spaces[i];
        break;
      }
      // Check for win by row
      j = i * 3;
      if (spaces[j] && spaces[j] == spaces[j + 1] && spaces[j] == spaces[j + 2]) {
        playerWin = spaces[j];
        break;
      }
    }
    // Check for win by diagonal from left-top to right-bottom
    if (!playerWin && spaces[0] && spaces[0] == spaces[4] && spaces[0] == spaces[8]) {
      playerWin = spaces[0];
    }
    // Check for win by diagonal from right-top to left-bottom
    if (!playerWin && spaces[2] && spaces[2] == spaces[4] && spaces[2] == spaces[6]) {
      playerWin = spaces[2];
    }
    if (playerWin) {
      state = STATE_WIN;
    }

    // Check if there is no winner and all spots are taken
    if (!playerWin && moves.length == 9) {
      state = STATE_NO_WIN;
    }
  }

  /* game logic */

  function nextPlayer(newPlayer) {
    if (newPlayer) {
      player = newPlayer;
    }
    return player;
  }

  function moveValid(x, y) {
    var pos = (y * 3) + x;
    if (state != STATE_PLAY) {
      return false;
    }
    if (spaces[pos]) {
      return false;
    }
    return true;
  }

  /* return public methods and variables */

  return {
    /* variables */
    'STATE_PLAY': STATE_PLAY,
    'STATE_WIN': STATE_WIN,
    'STATE_NO_WIN': STATE_NO_WIN,

    /* methods */
    'getMoves': function () {
      return moves;
    },
    'getPlayerWin': function () {
      return playerWin;
    },
    'getSpaces': function () {
      return spaces;
    },
    'getState': function () {
      return state;
    },
    'nextPlayer': nextPlayer,
    'move': move,
    'moveValid': moveValid,
    'setMoves': function (newMoves) {
      if (newMoves) {
        moves = newMoves;
      }
    },
    'setSpaces': function (newSpaces) {
      if (newSpaces) {
        spaces = newSpaces;
      }
    }
  };
};

// for use as a node.js module
var exports = exports || {};
if (!exports.hasOwnProperty('game')) {
  exports.game = nodejsTicGame;
}
